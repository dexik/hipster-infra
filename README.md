# Инфраструтктура k8s для микросервисного приложения Hipster Shop

## Особенности
  - Используется [Helmfile](https://github.com/roboll/helmfile)
  - Возможность реализации для разных окружения (prod, dev ...)
  - Используются заранее подготовленные ресурсы HelmRelease для Flux и Flux Helm Operator
  - Возможно добавление кастомных манифестов для применения в кластере
  - Блэкбокс мониторинг приложения
  - Кастомные дашборды Grafana для блэкбокс мониторинга, Nginx-Ingress
  - Оповещения о проблемах в Slack

## Что будет установлено
  - [Cert-Manager](https://cert-manager.io/) для выпуска сертификатов Let's Encrypt
  - [Flux](https://fluxcd.io/) для GitOps
  - [Flux Helm Operator](https://github.com/fluxcd/helm-operator) для GitOps и Helm
  - [Nginx-Ingress](https://kubernetes.github.io/ingress-nginx/)
  - [Prometheus Operator](https://github.com/prometheus-operator/prometheus-operator) для мониторинга
  - [Blackbox Exporter](https://github.com/prometheus/blackbox_exporter) для блэкбокс мониторинга

## Необходимые переменные окружения
  - `KUBE_CONFIG` - base64 закодированные конфиг для Kubectl. Обратите внимание, что имя контекста должно совпадать с именем ветки, в которой происходит сборка
  - `SLACK_WEBHOOK_URL` - вебхук для отправки уведомлений в Slack

## Сервисы инфраструктуры будут доступны
  - Grafana: `https://grafana.<environment>.00aa.ru`
  - Alertmanager: `https://alertmanager.<environment>.00aa.ru`
  - Prometheus: `https://prometheus.<environment>.00aa.ru`

## TODO
  - Логирование в кластере